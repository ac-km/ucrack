package com.virjar.ucrack.hermes.weishi;

import com.virjar.hermes.hermesagent.hermes_api.ActionRequestHandler;
import com.virjar.hermes.hermesagent.hermes_api.WrapperAction;
import com.virjar.hermes.hermesagent.hermes_api.aidl.InvokeRequest;
import com.virjar.hermes.hermesagent.hermes_api.aidl.InvokeResult;
import com.virjar.xposed_extention.ClassLoadMonitor;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

import de.robv.android.xposed.XposedHelpers;

@WrapperAction("GetUsers")
public class GetUsersHandler implements ActionRequestHandler {
    @Override
    public Object handleRequest(InvokeRequest invokeRequest) {
        String userID = invokeRequest.getString("userID");
        if (StringUtils.isBlank(userID)) {
            return InvokeResult.failed("the param {userID} not presented");
        }
        String attach_info = invokeRequest.getString("attach_info");
        if (attach_info == null) {
            attach_info = "";
        }
        String type = invokeRequest.getString("type");
        if (!StringUtils.equalsIgnoreCase(type, "follower")
                && !StringUtils.equalsIgnoreCase(type, "interester")) {
            type = "follower";
        }
        Object uniqueID;

        Object stGetUsersReq = XposedHelpers.newInstance(ClassLoadMonitor.tryLoadClass("NS_KING_INTERFACE.stGetUsersReq"), attach_info, userID, type, (ArrayList) null);

        Class requestBeanClass = ClassLoadMonitor.tryLoadClass("com.tencent.oscar.module.f.a.f$18");
        if (requestBeanClass == null) {
            requestBeanClass = ClassLoadMonitor.tryLoadClass("com.tencent.oscar.module.e.a.g$21");
            uniqueID = XposedHelpers.callStaticMethod(ClassLoadMonitor.tryLoadClass("com.tencent.oscar.base.utils.u"), "a");
        } else {
            uniqueID = XposedHelpers.callStaticMethod(ClassLoadMonitor.tryLoadClass("com.tencent.oscar.base.utils.Utils"), "generateUniqueId");
        }
        if (requestBeanClass == null) {
            return InvokeResult.failed("can not load request bean class, app protocol maybe upgrade");
        }

        Object requestBean = XposedHelpers.newInstance(requestBeanClass, uniqueID, "GetUsers");
        XposedHelpers.setObjectField(requestBean, "req", stGetUsersReq);
        return WeishiUtil.sendRequest(requestBean);
    }
}
