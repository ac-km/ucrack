package com.virjar.ucrack.contentprovider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author lei.X
 * @date 2019/4/15 2:22 PM
 */
public class DBHelper extends SQLiteOpenHelper {


    // 数据库名
    private static final String DATABASE_NAME = "ucrack.db";

    //数据库版本号
    private static final int DATABASE_VERSION = 1;


    public DBHelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
