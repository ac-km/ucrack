package com.virjar.ucrack.host;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.virjar.ucrack.R;
import com.virjar.ucrack.plugin.SharePreferenceConfigHolder;
import com.virjar.ucrack.plugin.ToolConstant;
import com.virjar.ucrack.plugin.UcrackProviderUtil;

import java.util.HashMap;
import java.util.Map;

import static com.virjar.ucrack.plugin.SharePreferenceConfigHolder.ALIAPY_STACK_TRACE;
import static com.virjar.ucrack.plugin.SharePreferenceConfigHolder.D_SWORD;
import static com.virjar.ucrack.plugin.SharePreferenceConfigHolder.IJIAMI_PREVENT;
import static com.virjar.ucrack.plugin.SharePreferenceConfigHolder.KILL_SELF;
import static com.virjar.ucrack.plugin.SharePreferenceConfigHolder.LOG;
import static com.virjar.ucrack.plugin.SharePreferenceConfigHolder.MONITOR_STACK;
import static com.virjar.ucrack.plugin.SharePreferenceConfigHolder.NATIVE_FUNTION_REGISTERY;
import static com.virjar.ucrack.plugin.SharePreferenceConfigHolder.NETDATA_PRINT;
import static com.virjar.ucrack.plugin.SharePreferenceConfigHolder.SWITCH;
import static com.virjar.ucrack.plugin.SharePreferenceConfigHolder.SonnyJackDragView;
import static com.virjar.ucrack.plugin.SharePreferenceConfigHolder.TRUST_ME;
import static com.virjar.ucrack.plugin.SharePreferenceConfigHolder.UNPACK_VERSION_1;
import static com.virjar.ucrack.plugin.SharePreferenceConfigHolder.UNPACK_VERSION_3;
import static com.virjar.ucrack.plugin.SharePreferenceConfigHolder.WEB_VIEW;


public class DetailConfigActivity extends AppCompatActivity {

    private static final String TAG = "xulei.xu_INFO";

    UcrackProviderUtil ucrackProvider;

    String appPackage;

    private Map<String,Boolean> config;


    @SuppressLint("WorldReadableFiles")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Intent intent = getIntent();
        String appName = intent.getStringExtra(ToolConstant.appName);
        appPackage = intent.getStringExtra(ToolConstant.appPackage);
        this.setTitle(appName + "插件开关");



        ucrackProvider = UcrackProviderUtil.getSingleton(this);

        //初始化app Propertity属性
        initAppPropertity(appPackage);
        Log.i(TAG, "appName:{}" + appName + "--appPackage:{}" + appPackage);
        buildCheckBoxLister();
    }


    private void bindCheckBox(Integer checkBoxId, final String switchName) {
        CheckBox switchFLag = (CheckBox) findViewById(checkBoxId);
        switchFLag.setChecked(config.get(switchName));

        switchFLag.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ucrackProvider.updateAppPropertity(appPackage,switchName,isChecked);
            }
        });

    }


    //构建checkBox响应
    private void buildCheckBoxLister() {
        bindCheckBox(R.id.ckb_switch, SharePreferenceConfigHolder.SWITCH);
        bindCheckBox(R.id.ckb_unpack_1, SharePreferenceConfigHolder.UNPACK_VERSION_1);
        bindCheckBox(R.id.ckb_unpack_3, SharePreferenceConfigHolder.UNPACK_VERSION_3);
        bindCheckBox(R.id.ckb_log, SharePreferenceConfigHolder.LOG);
        bindCheckBox(R.id.ckb_netDataPrint, SharePreferenceConfigHolder.NETDATA_PRINT);
        bindCheckBox(R.id.ckb_webview, SharePreferenceConfigHolder.WEB_VIEW);
        bindCheckBox(R.id.ckb_dsword, SharePreferenceConfigHolder.D_SWORD);
        bindCheckBox(R.id.ckb_trust, SharePreferenceConfigHolder.TRUST_ME);
        bindCheckBox(R.id.monitorThread, SharePreferenceConfigHolder.MONITOR_STACK);
        bindCheckBox(R.id.ijiami_prevent, SharePreferenceConfigHolder.IJIAMI_PREVENT);
        bindCheckBox(R.id.self_kill_stacktrace, SharePreferenceConfigHolder.KILL_SELF);
        bindCheckBox(R.id.alipay_stack_trace, SharePreferenceConfigHolder.ALIAPY_STACK_TRACE);
        bindCheckBox(R.id.native_function_register, SharePreferenceConfigHolder.NATIVE_FUNTION_REGISTERY);
        bindCheckBox(R.id.sonny_jack_drag_view, SharePreferenceConfigHolder.SonnyJackDragView);
    }

    private void initAppPropertity(String appName){
        Map<String,Boolean> defaultValue = new HashMap<>();
        defaultValue.put(SWITCH, false);
        defaultValue.put(UNPACK_VERSION_1, false);
        defaultValue.put(UNPACK_VERSION_3, false);
        defaultValue.put(LOG, true);
        defaultValue.put(NETDATA_PRINT, true);
        defaultValue.put(WEB_VIEW, false);
        defaultValue.put(D_SWORD, true);
        defaultValue.put(TRUST_ME, true);
        defaultValue.put(MONITOR_STACK, true);
        defaultValue.put(IJIAMI_PREVENT, false);
        defaultValue.put(KILL_SELF, true);
        defaultValue.put(ALIAPY_STACK_TRACE, false);
        defaultValue.put(NATIVE_FUNTION_REGISTERY, false);
        defaultValue.put(SonnyJackDragView, true);

        Map<String,Boolean> b = ucrackProvider.queryMapWithAppName(appName);

        defaultValue.putAll(b);

        config = defaultValue;

    }


}
